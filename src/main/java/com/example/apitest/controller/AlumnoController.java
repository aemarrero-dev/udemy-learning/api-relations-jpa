package com.example.apitest.controller;

import com.example.apitest.entity.Alumno;
import com.example.apitest.service.IAlumnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/alumnos")
public class AlumnoController {

    @Autowired
    private IAlumnoService alumnoService;

    @GetMapping(value = "/list", produces = "application/json")
    public List<Alumno> listarAlumnos() {
        return alumnoService.findAll();
    }

    @PostMapping(value = "/save", produces = "application/json")
    public Alumno guardar(@RequestBody Alumno alumno) {
        alumnoService.save(alumno);
        return alumno;
    }
}
