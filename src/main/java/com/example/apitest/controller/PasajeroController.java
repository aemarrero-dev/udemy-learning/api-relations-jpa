package com.example.apitest.controller;

import com.example.apitest.entity.Pasajero;
import com.example.apitest.service.IPasajeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/pasajeros")
public class PasajeroController {

    @Autowired
    private IPasajeroService pasajeroService;

    @GetMapping(value = "/list", produces = "application/json")
    public List<Pasajero> listarPasajeros() {
        return pasajeroService.findAll();
    }

    @PostMapping(value="/save", produces = "application/json")
    public Pasajero guardarPasajero(@RequestBody Pasajero pasajero) {
        pasajeroService.guardar(pasajero);
        return pasajero;
    }

    @PutMapping(value = "/save", produces = "application/json")
    public Pasajero update(@RequestBody Pasajero pasajero){
        pasajeroService.guardar(pasajero);
        return pasajero;
    }

    @DeleteMapping(value = "/delete/{id}", produces = "application/json")
    public String eliminar(@PathVariable("id") Integer id) {
        pasajeroService.eliminar(id);
        return "Registro eliminado";
    }
}
