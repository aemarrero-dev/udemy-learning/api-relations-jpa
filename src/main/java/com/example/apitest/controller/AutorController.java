package com.example.apitest.controller;

import com.example.apitest.entity.Autor;
import com.example.apitest.service.IAutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/autores")
public class AutorController {

    @Autowired
    private IAutorService autorService;

    @GetMapping(value = "/list", produces = "application/json")
    public List<Autor> listarAutores() {
        return autorService.findAll();
    }

    @PostMapping(value = "/save", produces = "application/json")
    public Autor guardar(@RequestBody Autor autor) {
        autorService.save(autor);
        return autor;
    }

    @DeleteMapping(value = "/delete/{id}", produces = "application/json")
    public String eliminar(@PathVariable("id") Integer id) {
        autorService.deleteById(id);
        return "Registro eliminado";
    }
}
