package com.example.apitest.controller;

import com.example.apitest.entity.Profesor;
import com.example.apitest.service.IProfesorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "profesores")
public class ProfesorController {

    @Autowired
    private IProfesorService profesorService;

    @GetMapping(value = "/list", produces = "application/json")
    public List<Profesor> listarProfesores () {
        return profesorService.findAll();
    }

    @PostMapping(value = "/save", produces = "application/json")
    public Profesor guardar(@RequestBody Profesor profesor) {
        profesorService.save(profesor);
        return profesor;
    }
}
