package com.example.apitest;

import com.example.apitest.entity.Autor;
import com.example.apitest.entity.Libro;
import com.example.apitest.entity.Pasajero;
import com.example.apitest.entity.Pasaporte;
import com.example.apitest.repository.IPasajeroRepository;
import com.example.apitest.repository.IPasaporteRepository;
import com.example.apitest.service.IAutorService;
import com.example.apitest.service.IPasajeroService;
import com.example.apitest.service.IPasaporteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ApiTestApplication implements CommandLineRunner {

	@Autowired
	IPasajeroService pasajeroService;

	@Autowired
	IPasaporteService pasaporteService;

	@Autowired
	IAutorService autorService;

	public static void main(String[] args) {
		SpringApplication.run(ApiTestApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

/*
		Autor p = new Autor();
		p.setNombre("J. R. Tolkien");
		p.setEdad(54);

		List<Libro> libros = new ArrayList<>();
		Libro l1 = new Libro();
		l1.setNombre("El Hobbit");
		l1.setDescripcion("Un viaje inesperado");
		l1.setPaginas(450);
		l1.setAutor(p);

		Libro l2 = new Libro();
		l2.setNombre("El Hobbit 2");
		l2.setDescripcion("La Desolacion de Smaug");
		l2.setPaginas(380);
		l2.setAutor(p);

		libros.add(l1);
		libros.add(l2);

		p.setLibros(libros);

		autorService.save(p);*/
	}
}
