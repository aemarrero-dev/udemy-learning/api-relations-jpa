package com.example.apitest.repository;

import com.example.apitest.entity.Libro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ILibroRepository extends JpaRepository<Libro, Integer> {
}
