package com.example.apitest.repository;

import com.example.apitest.entity.Autor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAutorRepository extends JpaRepository<Autor, Integer> {
}
