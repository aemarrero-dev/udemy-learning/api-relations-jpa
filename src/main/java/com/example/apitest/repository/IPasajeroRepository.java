package com.example.apitest.repository;

import com.example.apitest.entity.Pasajero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPasajeroRepository extends JpaRepository<Pasajero, Integer> {
}
