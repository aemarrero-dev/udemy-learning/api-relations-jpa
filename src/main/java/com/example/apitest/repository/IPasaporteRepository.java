package com.example.apitest.repository;

import com.example.apitest.entity.Pasaporte;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPasaporteRepository extends JpaRepository<Pasaporte, Integer> {
}
