package com.example.apitest.service;

import com.example.apitest.entity.Libro;

import java.util.List;

public interface ILibroService {

    List<Libro> findAll();
    void save(Libro libro);
    void deleteById(Integer id);
}
