package com.example.apitest.service;

import com.example.apitest.entity.Autor;

import java.util.List;

public interface IAutorService {
    List<Autor> findAll();
    void save(Autor autor);
    void deleteById(Integer id);
}
