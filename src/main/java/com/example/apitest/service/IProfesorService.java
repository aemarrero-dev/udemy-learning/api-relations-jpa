package com.example.apitest.service;

import com.example.apitest.entity.Profesor;

import java.util.List;

public interface IProfesorService {
    List<Profesor> findAll();
    void save(Profesor profesor);
    void deleteById(Integer id);
}
