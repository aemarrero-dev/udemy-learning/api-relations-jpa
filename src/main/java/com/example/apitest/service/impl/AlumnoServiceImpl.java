package com.example.apitest.service.impl;

import com.example.apitest.entity.Alumno;
import com.example.apitest.repository.IAlumnoRepository;
import com.example.apitest.service.IAlumnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlumnoServiceImpl implements IAlumnoService {

    @Autowired
    private IAlumnoRepository repo;

    @Override
    public List<Alumno> findAll() {
        return repo.findAll();
    }

    @Override
    public void save(Alumno alumno) {
        repo.save(alumno);
    }

    @Override
    public void deleteById(Integer id) {
        repo.deleteById(id);
    }
}
