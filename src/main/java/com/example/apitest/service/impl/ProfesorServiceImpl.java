package com.example.apitest.service.impl;

import com.example.apitest.entity.Profesor;
import com.example.apitest.repository.IProfesorRepository;
import com.example.apitest.service.IProfesorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfesorServiceImpl implements IProfesorService {

    @Autowired
    private IProfesorRepository repo;

    @Override
    public List<Profesor> findAll() {
        return repo.findAll();
    }

    @Override
    public void save(Profesor profesor) {
        repo.save(profesor);
    }

    @Override
    public void deleteById(Integer id) {
        repo.deleteById(id);
    }
}
