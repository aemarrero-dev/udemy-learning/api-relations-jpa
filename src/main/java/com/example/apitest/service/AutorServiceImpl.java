package com.example.apitest.service;

import com.example.apitest.entity.Autor;
import com.example.apitest.repository.IAutorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AutorServiceImpl implements IAutorService {

    @Autowired
    private IAutorRepository repo;

    @Override
    public List<Autor> findAll() {
        return repo.findAll();
    }

    @Override
    public void save(Autor autor) {
        repo.save(autor);
    }

    @Override
    public void deleteById(Integer id) {
        repo.deleteById(id);
    }
}
