package com.example.apitest.service;

import com.example.apitest.entity.Pasajero;
import com.example.apitest.repository.IPasajeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PasajeroServiceImpl implements IPasajeroService {

    @Autowired
    private IPasajeroRepository repo;

    @Override
    public void guardar(Pasajero pasajero) {
        repo.save(pasajero);
    }

    @Override
    public List<Pasajero> findAll() {
        return repo.findAll();
    }

    @Override
    public void eliminar(Integer id) {
        repo.deleteById(id);
    }
}
