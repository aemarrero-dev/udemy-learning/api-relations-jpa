package com.example.apitest.service;

import com.example.apitest.entity.Alumno;

import java.util.List;

public interface IAlumnoService {
    List<Alumno> findAll();
    void save(Alumno alumno);
    void deleteById(Integer id);
}
