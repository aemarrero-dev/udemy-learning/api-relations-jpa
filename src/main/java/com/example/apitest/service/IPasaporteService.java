package com.example.apitest.service;

import com.example.apitest.entity.Pasaporte;

import java.util.List;

public interface IPasaporteService {

    void guardar(Pasaporte pasaporte);
    List<Pasaporte> findAll();
}
