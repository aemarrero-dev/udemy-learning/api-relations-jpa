package com.example.apitest.service;

import com.example.apitest.entity.Pasaporte;
import com.example.apitest.repository.IPasaporteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PasaporteServiceImpl implements IPasaporteService {

    @Autowired
    private IPasaporteRepository repo;

    @Override
    public void guardar(Pasaporte pasaporte) {
        repo.save(pasaporte);
    }

    @Override
    public List<Pasaporte> findAll() {
        return repo.findAll();
    }


}
