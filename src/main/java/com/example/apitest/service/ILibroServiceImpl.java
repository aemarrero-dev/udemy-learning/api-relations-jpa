package com.example.apitest.service;

import com.example.apitest.entity.Libro;
import com.example.apitest.repository.ILibroRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ILibroServiceImpl implements ILibroService {

    @Autowired
    private ILibroRepository repo;

    @Override
    public List<Libro> findAll() {
        return repo.findAll();
    }

    @Override
    public void save(Libro libro) {
        repo.save(libro);
    }

    @Override
    public void deleteById(Integer id) {
        repo.deleteById(id);
    }
}
