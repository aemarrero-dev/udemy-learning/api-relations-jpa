package com.example.apitest.service;

import com.example.apitest.entity.Pasajero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

public interface IPasajeroService {

    void guardar(Pasajero pasajero);
    List<Pasajero> findAll();
    void eliminar(Integer id);
}
